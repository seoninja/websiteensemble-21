<?php get_header(); 


if( get_locale() == 'nl_NL')
	{
$lng=array(

	'subscribeTitle'=>'Blijf op de hoogte met deskundige tips',
	'subscribeText'=>'Voer uw e-mailadres in om een melding te ontvangen wanneer we nieuwe inhoud publiceren. Maak je geen zorgen, we zullen je informatie niet verkopen of je inschrijven op andere marketinglijsten.',
	'subscribeCta'=>'Aanmelden',
	'subscribePlc'=>'E-mailadres'
);
	$ln='nl';
}
else
{$lng=array(

	'subscribeTitle'=>'Stay up to date with expert insights',
	'subscribeText'=>'Enter your email to be notified when we publish new content. Don’t worry, we won’t sell your information or subscribe you to any other marketing lists. ',
	'subscribeCta'=>'Subscribe',
	'subscribePlc'=>'Email'
);
$ln='en';
}



if(! is_singular('post'))
{
	
get_search_form(); 

}

?>
<?php
if(is_search() || is_archive() || is_category())
{
	?>
<div class="search my-5">
		<div class="container">
			<div class="row  justify-content-center">
				<div class="col-12 justify-content-center">
					
			<?php if(is_search()){
		
		?>
		
				<h1 class="text-center">Search results for: <?php echo get_search_query() ?></h1>
					<?php 
					global $wp_query;
					if ( $wp_query->found_posts ) {}
							else
							{
								?> 
									<h2 class="text-center">We could not find any results for your search.</h2><p class="text-center">You can give it another try through the search form above.</p>
									<?php }
			}
	
	if(is_archive())
	{
		?> <h1 class="text-center">
	<?php  the_archive_title(); ?></h1>
					<?php
		
	}
	
					 
?>
					
					
				</div>
			</div>
		</div>
	</div>

<?php
	
	
}

?>
	

<?php 
if ( have_posts()  && !is_single())
{
	
	?>

<div class="articles">
	<div class="container">
		<div class="row">
<?php
}
			$i = 0;
			if ( have_posts() ) : while ( have_posts() ) : the_post(); 
			
			if ( is_single() ) {
			
			get_template_part( 'template-parts/post_single', get_post_type() );
			} 
				else
				{
			if($i==0)
			{
			get_template_part( 'template-parts/post_main', get_post_type() );
			}else
			{
				get_template_part( 'template-parts/post_regular', get_post_type() );
			}
			$i++;
			}
			
			
			?>

		

<?php endwhile; endif;
			
			if ( have_posts()  && !is_single())
{
	
	?>
			<?php get_template_part( 'template-parts/pagination' ); ?>
</div></div></div>

<?php
}
		
			
			
			?>


								
							
<div class="subscribe py-5">
		<div class="container px-5">
			<div class="row justify-content-center">
				<div class="col-lg-8 bk-blue px-5 text-center"><h3 class="fw-bold pr-3 mt-5"><?php echo $lng['subscribeTitle'];?></h3>
				<p class="mb-5"><?php echo $lng['subscribeText'];?></p>
				<form class="p-2 mb-5">
					<div class="row justify-content-center response">
						<div class="col-xxl-8 col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
						
						<div class="d-flex bg-form p-3">
						<div class="flex-fill me-auto" >
							<input type="text" name="frmEm" placeholder="<?php echo $lng['subscribePlc'];?>" class="form-control">
							
						</div>
						<div class="flex-fill ms-auto text-end" >
							<a class="btn btn-blmarine" onclick="checkThis();"><?php echo $lng['subscribeCta'];?></a>
						</div></div></div>
						<div class="err"></div>
					
					</div>
					
					</form></div>
				
			</div>
		</div>
	
	</div>


<?php get_footer(); ?>